#!/usr/bin/env python3

import requests
import os
import re
from dotenv import load_dotenv

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')

if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')

url_for_comment = str(input('URL: '))
message = str(input('Message: '))

matches = re.search(r'wall(\d+)_(\d+)', url_for_comment)
if matches:
    owner_id = matches.group(1)
    post_id = matches.group(2)
    print("owner_id:", owner_id)
    print("item_id:", post_id)
else:
    print("Ссылка не соответствует ожидаемому формату.")

url = "https://api.vk.com/method/wall.createComment"

headers = {
    "User-Agent": "KateMobileAndroid/103-540 (Android 9; SDK 28; x86; Google AOSP on IA Emulator; en)",
    "Content-Type": "application/x-www-form-urlencoded"
}
data = {
    "access_token": ACCESS_TOKEN, 
    "message": message,
    "owner_id": owner_id,
    "post_id": post_id,
    "v": "5.131"
}

response = requests.post(url, headers=headers, data=data)
print(response.text)

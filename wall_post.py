#!/usr/bin/env python3

import requests
import os
from dotenv import load_dotenv

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')

if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')

url = "https://api.vk.com/method/wall.post"

owner_id = int(input('Owner ID: '))
message = str(input('Message: '))

payload = {
    "access_token": ACCESS_TOKEN, # https://vkhost.github.io
    "message": message,
    "owner_id": owner_id,
    "services": "twitter,facebook",
    "v": "5.131"
}

headers = {
    "User-Agent": "KateMobileAndroid/103-540 (Android 9; SDK 28; x86; Google AOSP on IA Emulator; en)",
    "Content-Type": "application/x-www-form-urlencoded",
    "Accept-Encoding": "gzip, deflate"
}

response = requests.post(url, data=payload, headers=headers)

print(response.text)

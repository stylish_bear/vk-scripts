#!/usr/bin/env python3

import requests
import random
import os
from dotenv import load_dotenv

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')

if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')

vk_id = int(input('Vk ID: '))
message_text = str(input('Message: '))
random_id = random.randint(10**12, (10**13)-1)

url = 'https://api.vk.com/method/messages.send'

params = {
    'access_token': ACCESS_TOKEN, # https://vkhost.github.io
    'message': message_text,
    'peer_id': vk_id,
    'random_id': random_id,
    'v': '5.131'
}

headers = {
    'User-Agent': 'KateMobileAndroid/103-540 (Android 9; SDK 28; x86; Google AOSP on IA Emulator; en)',
    'Accept-Encoding': 'gzip, deflate',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Connection': 'Keep-Alive',
    'Content-Length': '304'
}

response = requests.post(url, data=params, headers=headers)
if __name__ == '__main__':
    if response.status_code == 200:
        print('Запрос успешно выполнен!')
        data = response.json()
        print(data)
    else:
        print('Произошла ошибка при выполнении запроса. Код ошибки:', response.status_code)

#!/usr/bin/env python3

import requests
import time
import os
from dotenv import load_dotenv

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')

if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')

url = 'https://api.vk.com/method/account.setOnline'

params = {
    'access_token': ACCESS_TOKEN, # https://vkhost.github.io
    'v': '5.131'
}

headers = {
    'User-Agent': 'KateMobileAndroid/103-540 (Android 9; SDK 28; x86; Google AOSP on IA Emulator; en)',
    'Accept-Encoding': 'gzip, deflate',
    'Connection': 'Keep-Alive'
}

if __name__ == '__main__':
    while True:
        response = requests.get(url, params=params, headers=headers)

        if response.status_code == 200:
            print('Запрос успешно выполнен!')
            data = response.json()
            print(data)
        else:
            print('Произошла ошибка при выполнении запроса. Код ошибки:', response.status_code)
            exit()

        time.sleep(60)

#!/usr/bin/env python3

import requests
import os
import re
from dotenv import load_dotenv

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')

if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')

url_for_like = str(input('URL: '))

matches = re.search(r'wall(\d+)_(\d+)', url_for_like)
if matches:
    owner_id = matches.group(1)
    item_id = matches.group(2)
    print("owner_id:", owner_id)
    print("item_id:", item_id)
else:
    print("Ссылка не соответствует ожидаемому формату.")

url = "https://api.vk.com/method/likes.add"

params = {
    "access_token": ACCESS_TOKEN, # https://vkhost.github.io
    "item_id": item_id,
    "owner_id": owner_id,
    "type": "post",
    "v": "5.157"
}

headers = {
    "User-Agent": "KateMobileAndroid/103-540 (Android 9; SDK 28; x86; Google AOSP on IA Emulator; en)",
    "Accept-Encoding": "gzip, deflate"
}

response = requests.get(url, params=params, headers=headers)

print(response.text)

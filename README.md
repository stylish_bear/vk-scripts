# VK scripts

The bunch of scripts for vk to emulate user actions

## Getting started

Just copy this repo and use this scripts

```
git clone https://gitlab.com/stylish_bear/vk-scripts.git
cd vk-scripts
```

Create `.env` file to securely store Vk access token(before u can get it on this [page](https://vkhost.github.io)). Ur `.env` should be like this

```
ACCESS_TOKEN = "your tokeen"
```

And just run scripts using python

```
python3 <script-name>
```
## Authors and acknowledgment
[Vadim](https://t.me/stylish_bear)

## License
Cannot be used without specifying the author

## Project status
It's ok

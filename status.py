#!/usr/bin/env python3

import requests
import os
from dotenv import load_dotenv

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')

if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')

url = "https://api.vk.com/method/status.set"
text = str(input('Status: '))

params = {
    "access_token": ACCESS_TOKEN,
    "text": text,
    "v": "5.131"
}

headers = {
    "User-Agent": "KateMobileAndroid/103-540 (Android 9; SDK 28; x86; Google AOSP on IA Emulator; en)",
    "Accept-Encoding": "gzip, deflate"
}

response = requests.get(url, params=params, headers=headers)
print(response.text)
